//
//  BundleExt.swift
//  MyWeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation

extension Bundle {
    func getInfoByKey(key: String) -> Any? {
        guard let result = Bundle.main.infoDictionary?[key] else {
            return nil
        }
        return result
    }
}
