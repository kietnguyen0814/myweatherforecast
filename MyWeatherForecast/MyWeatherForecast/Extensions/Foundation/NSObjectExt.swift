//
//  NSObjectExt.swift
//  WeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation

extension NSObject {
    class func name() -> String {
        // get class name
        let path = NSStringFromClass(self)
        
        return path.components(separatedBy: ".").last ?? ""
    }
}
