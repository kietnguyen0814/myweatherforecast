//
//  StringExt.swift
//  MyWeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation
import Alamofire

extension String {
    /// Encode a String to Base64
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    /// Decode a String from Base64. Returns nil if unsuccessful.
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
    func encodeUrlString(parameters: Parameters?) -> String {
        guard let url = URL(string: self),
              var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false),
              let parameters = parameters else {
            return self
        }
        urlComponents.queryItems = []
        for name in parameters.keys.sorted() {
            if let value = parameters[name] {
                let item = URLQueryItem(
                    name: "\(name)",
                    value: "\(value)"
                )
                urlComponents.queryItems?.append(item)
            }
        }
        
        return urlComponents.url?.absoluteString ?? self
    }
}
