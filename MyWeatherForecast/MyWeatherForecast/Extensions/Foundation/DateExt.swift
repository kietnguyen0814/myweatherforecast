//
//  DateExt.swift
//  MyWeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation

// A Date extension for making custom date&time from timestamp or epoch
enum CustomDateStyle: String {
    case mmdd = "MM/dd"
    case MMddYY = "MM/dd/YY"
    case ha = "h a"
    case hmmssa = "h:mm:ss a"
    case yyyymmdd = "YYYY/MM/dd"
    case HHmm = "HH:mm"
    case E = "E"
    case EdMMMyyyy = "E, d MMM yyyy"
    case EEEEMMMdyyyy = "EEEE, MMM d, yyyy"
}

extension Date {
    func toString(format: String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = format
        return dateformatter.string(from: self)
    }
}
