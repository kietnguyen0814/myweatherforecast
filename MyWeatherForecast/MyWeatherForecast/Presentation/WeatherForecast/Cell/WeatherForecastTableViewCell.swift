//
//  WeatherForecastTableViewCell.swift
//  WeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import UIKit

class WeatherForecastTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAvgTemp: UILabel!
    @IBOutlet weak var lblPressure: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupAccessibility()
        applyScaleText()
    }
    
    private func setupAccessibility() {
        self.isAccessibilityElement = true
    }
    
    private func applyScaleText() {
        lblDate.font = UIFont.preferredFont(forTextStyle: .body)
        lblDate.adjustsFontForContentSizeCategory = true
        
        lblAvgTemp.font = UIFont.preferredFont(forTextStyle: .body)
        lblAvgTemp.adjustsFontForContentSizeCategory = true
        
        lblPressure.font = UIFont.preferredFont(forTextStyle: .body)
        lblPressure.adjustsFontForContentSizeCategory = true
        
        lblHumidity.font = UIFont.preferredFont(forTextStyle: .body)
        lblHumidity.adjustsFontForContentSizeCategory = true
        
        lblDesc.font = UIFont.preferredFont(forTextStyle: .body)
        lblDesc.adjustsFontForContentSizeCategory = true
    }
    
    func configCell(weather: AllWeekWeather) {
        lblDate.text = "Date: \(weather.strShortDate)"
        lblAvgTemp.text = "Average Temperature: \(Int(weather.temp.day))ºC"
        lblPressure.text = "Pressure: \(weather.pressure)"
        lblHumidity.text = "Humidity: \(weather.humidity)%"
        guard let weatherDetail = weather.weather.first else {
            print("weather is empty")
            return
        }
        lblDesc.text = "Description: \(weatherDetail.description)"
        self.accessibilityLabel = "On \(weather.strFullDate), the weather is \(weatherDetail.description) with average temperature is \(Int(weather.temp.day))ºC, pressure is \(weather.pressure) atm, humidity is \(weather.humidity)%"
    }
    
}
