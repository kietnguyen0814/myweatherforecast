//
//  WeatherForecastViewController.swift
//  WeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import UIKit

class WeatherForecastViewController: UIViewController {

    @IBOutlet weak var txtSearch: UISearchBar!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet var infoView: UIView!
    @IBOutlet weak var imvIssue: UIImageView!
    @IBOutlet weak var lblIssueInfo: UILabel!
    @IBOutlet weak var lblIssueDetail: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    var presenter: WeatherForecastPresenterProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = WeatherForecastPresenter(view: self, weatherService: WeatherService(apiClient: ApiClient()))
        setupNavigationBar()
        setupTableView()
        configInfoView()
        setupAccessibility()
    }

}

extension WeatherForecastViewController {
    private func setupNavigationBar() {
        navigationItem.title = "Weather Forecast"
    }
    
    private func setupTableView() {
        myTableView.tableFooterView = UIView()
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableView.automaticDimension
        myTableView.keyboardDismissMode = .onDrag
        myTableView.register(UINib(nibName: WeatherForecastTableViewCell.name(), bundle: nil), forCellReuseIdentifier: WeatherForecastTableViewCell.name())
        myTableView.dataSource = self
    }
    
    private func configInfoView() {
        infoView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        infoView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        containView.addSubview(infoView)
    }
    
    private func setupAccessibility() {
        for subview in txtSearch.subviews {
            for subSubViews in subview.subviews {
                for child in subSubViews.subviews {
                    if let textField = child as? UISearchTextField {
                        textField.isAccessibilityElement = true
                        textField.accessibilityLabel = "You're searching with keyword \(textField.text ?? "")"
                    }
                }
            }
        }
    }
}

extension WeatherForecastViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numbersWeatherInSection ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WeatherForecastTableViewCell.name(), for: indexPath) as? WeatherForecastTableViewCell
        if let weather = presenter?.weatherForRowIndexPath(indexPath: indexPath) {
            cell?.configCell(weather: weather)
        }
        return cell ?? UITableViewCell.init()
    }
}

extension WeatherForecastViewController: WeatherForecastView {
    func displayWeather() {
        DispatchQueue.main.async { [weak self] in
            self?.infoView.isHidden = true
            self?.myTableView.reloadData()
        }
    }
    
    func showLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicatorView.startAnimating()
        }
    }
    
    func hiddenLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicatorView.stopAnimating()
        }
    }
    
    func showErrorView(errorInfo: [String]) {
        DispatchQueue.main.async { [weak self] in
            self?.myTableView.reloadData()
            self?.infoView.isHidden = false
            self?.lblIssueInfo.text = errorInfo.first
            self?.lblIssueDetail.text = errorInfo[1]
            self?.imvIssue.image = UIImage(named: errorInfo.last ?? "ic_error")
        }
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

extension WeatherForecastViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.txtSearch.resignFirstResponder()
        guard let searchText = searchBar.text, !searchText.isEmpty, searchText.count > 2 else {
            presenter?.showInvalidInput()
            return
        }
        presenter?.fetchDailyWeather(with: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.txtSearch.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("Begin to search")
    }
}
