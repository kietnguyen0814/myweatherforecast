//
//  WeatherForecastPresenter.swift
//  MyWeatherForecast
//
//  Created by Kiet Nguyen on 3/5/22.
//

import Foundation
import Alamofire

protocol WeatherForecastView: AnyObject {
    func displayWeather()
    func showErrorView(errorInfo: [String])
    func showLoading()
    func hiddenLoading()
    func showAlert(title: String, message: String)
}

protocol WeatherForecastPresenterProtocol {
    var numbersWeatherInSection: Int { get }
    
    func showInvalidInput()
    
    func weatherForRowIndexPath(indexPath: IndexPath) -> AllWeekWeather?
    
    func fetchDailyWeather(with query: String)
}

final class WeatherForecastPresenter {
    private let unit = "metric"
    private let count = 7
    private let exclude = "daily,minutely,current,alerts"
    var listWeathers = [AllWeekWeather]()
    
    private let allWeatherService: WeatherServiceProtocol
    private weak var view: WeatherForecastView?
    
    init(view: WeatherForecastView, weatherService: WeatherServiceProtocol) {
        self.view = view
        self.allWeatherService = weatherService
    }
}

extension WeatherForecastPresenter: WeatherForecastPresenterProtocol {
    func showInvalidInput() {
        view?.showAlert(title: "Notification", message: "The city's name must be from 3 characters or above.")
    }
    
    func fetchDailyWeather(with query: String) {
        fetchDailyWeather(with: query, apiKey: NetworkConstants.openWeatherAPIKey)
    }
    
    func fetchDailyWeather(with query: String, apiKey: String) {
        if let apiKey = Bundle.main.getInfoByKey(key: apiKey) as? String {
            let param: Parameters = [
                "q": query.lowercased(),
                "cnt": count,
                "appid": apiKey,
                "units": unit
            ]
            view?.showLoading()
            allWeatherService.fetchAllWeekWeather(fromParams: param) { [weak self] result in
                self?.view?.hiddenLoading()
                switch result {
                case .success(let response):
                    self?.listWeathers = response.list.sorted(by: { $0.dt < $1.dt })
                    self?.view?.displayWeather()
                case .failure(let error):
                    self?.listWeathers = []
                    if error == .noData {
                        print("Response doesn't have data")
                        self?.view?.showErrorView(errorInfo: ["No place found", "There is no place match with your filter\rPlease try again", "ic_no_place"])
                    } else {
                        print(error.localizedDescription)
                        self?.view?.showErrorView(errorInfo: ["Error", "Something went wrong\rPlease try again", "ic_error"])
                    }
                }
            }
        } else {
            print("API Key not exist")
            self.view?.showErrorView(errorInfo: ["Error", "Something went wrong\rPlease try again", "ic_error"])
        }
    }
    
    func weatherForRowIndexPath(indexPath: IndexPath) -> AllWeekWeather? {
        if indexPath.row >= 0 &&
            indexPath.row < listWeathers.count {
            return listWeathers[indexPath.row]
        }
        return nil
    }
    
    var numbersWeatherInSection: Int {
        return listWeathers.count
    }
}
