//
//  AllWeekWeather.swift
//  MyWeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation

struct AllWeekWeatherResponse: Codable {
    let list: [AllWeekWeather]
}

struct AllWeekWeather: Codable {
    let dt: Int64
    let temp: Temperature
    let pressure: Int
    let humidity: Int
    let speed: Double //m/s
    let weather: [Weather]
    var strShortDate: String {
        let date = Date(timeIntervalSince1970: TimeInterval(dt))
        return date.toString(format: CustomDateStyle.EdMMMyyyy.rawValue)
    }
    
    var strFullDate: String {
        let date = Date(timeIntervalSince1970: TimeInterval(dt))
        return date.toString(format: CustomDateStyle.EEEEMMMdyyyy.rawValue)
    }
}

struct Weather: Codable {
    let id: Int
    let main: String
    let description: String
}

struct Temperature: Codable {
    let day: Double
    let min: Double
    let max: Double
}
