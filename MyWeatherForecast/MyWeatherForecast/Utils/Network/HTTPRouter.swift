//
//  HTTPRouter.swift
//  WeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation

enum WeatherRouter {
    case current
    case allWeek
    
    var url: String {
        var routePath: String {
            switch self {
            case .current:
                return "/data/2.5/onecall"
            case .allWeek:
                return "/data/2.5/forecast/daily"
            }
        }
        return NetworkConstants.baseWeatherURL + routePath
    }
}
