//
//  CacheManager.swift
//  MyWeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation
import UIKit

class Entry: NSObject, NSCoding, NSSecureCoding {
    
    static var supportsSecureCoding: Bool {
        return true
    }
    
    let expirationDate: Date
    
    let value: AnyObject
    
    public init(value: AnyObject, expirationDate: Date) {
        self.value = value
        self.expirationDate = expirationDate
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(value, forKey: "value")
        coder.encode(expirationDate, forKey: "expirationDate")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.value = aDecoder.decodeObject(forKey: "value")! as AnyObject
        self.expirationDate = aDecoder.decodeObject(forKey:"expirationDate") as! Date
    }
}

open class DataCache {
    static let cacheDirectoryPrefix = "com.nak.cache."
    static let ioQueuePrefix = "com.nak.queue."
    static let defaultMaxCachePeriodInSecond: TimeInterval = 60 * 5// 5 minute
    
    public static let shareInstance = DataCache(name: "default")
    
    let cachePath: String
    
    let memCache = NSCache<AnyObject, AnyObject>()
    let ioQueue: DispatchQueue
    let fileManager: FileManager
    
    /// Name of cache
    open var name: String = ""
    
    /// Life time of disk cache, in second. Default is a week
    open var maxCachePeriodInSecond = DataCache.defaultMaxCachePeriodInSecond
    
    /// Specify distinc name param, it represents folder name for disk cache
    private init(name: String, path: String? = nil) {
        self.name = name
        
        var cachePath = path ?? NSSearchPathForDirectoriesInDomains(.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!
        cachePath = (cachePath as NSString).appendingPathComponent(DataCache.cacheDirectoryPrefix + name)
        self.cachePath = cachePath
        
        ioQueue = DispatchQueue(label: DataCache.ioQueuePrefix + name)
        
        self.fileManager = FileManager()
    }

}

// MARK: - Store data

extension DataCache {
    
    /// Write data for key. This is an async operation.
    public func write(data: Data, forKey key: String) {
        let entry = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? Entry
        memCache.setObject(entry as AnyObject, forKey: key as AnyObject)
        writeDataToDisk(data: data, key: key)
    }
    
    private func writeDataToDisk(data: Data, key: String) {
        ioQueue.async {
            if self.fileManager.fileExists(atPath: self.cachePath) == false {
                do {
                    try self.fileManager.createDirectory(atPath: self.cachePath, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    print("DataCache: Error while creating cache folder: \(error.localizedDescription)")
                }
            }
            
            self.fileManager.createFile(atPath: self.cachePath(forKey: key), contents: data, attributes: nil)
        }
    }
    
    /// Read data for key
    public func readData(forKey key: String) -> Data? {
        if let entry = memCache.object(forKey: key as AnyObject) as? Entry {
            guard Date() < entry.expirationDate else {
                clean(byKey: key)
                return nil
            }
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: entry.value, requiringSecureCoding: true)
                return data
            } catch {
                return nil
            }
        } else {
            //if no data on memory then it will load data from disk
            if let dataFromDisk = readDataFromDisk(forKey: key),
               let entry = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(dataFromDisk) as? Entry {
                guard Date() < entry.expirationDate else {
                    clean(byKey: key)
                    return nil
                }
                memCache.setObject(entry as AnyObject, forKey: key as AnyObject)
                do {
                    let data = try NSKeyedArchiver.archivedData(withRootObject: entry.value, requiringSecureCoding: true)
                    return data
                } catch {
                    return nil
                }
            } else {
                return nil
            }
        }
    }
    
    /// Read data from disk for key
    public func readDataFromDisk(forKey key: String) -> Data? {
        return self.fileManager.contents(atPath: cachePath(forKey: key))
    }
    
    // MARK: - Read & write primitive types
    
    /// Write an object for key. This object must inherit from `NSObject` and implement `NSCoding` protocol. `String`, `Array`, `Dictionary` conform to this method.
    public func write(object: NSCoding, forKey key: String) {
        do {
            let date = Date().addingTimeInterval(self.maxCachePeriodInSecond)
            let entry = Entry(value: object, expirationDate: date)
            let entryData = try NSKeyedArchiver.archivedData(withRootObject: entry, requiringSecureCoding: true)
            write(data: entryData, forKey: key)
        } catch let error {
            print(error)
            return
        }
    }
    
    /// Write a string for key
    public func write(string: String, forKey key: String) {
        write(object: string as NSCoding, forKey: key)
    }
    
    /// Read an object for key. This object must inherit from `NSObject` and implement NSCoding protocol. `String`, `Array`, `Dictionary` conform to this method
    public func readObject(forKey key: String) -> NSObject? {
        let data = readData(forKey: key)
        
        if let data = data {
            do {
                let unarchivedData = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? NSObject
                return unarchivedData
            } catch {
                return nil
            }
        }
        
        return nil
    }
    
    /// Read a string for key
    public func readString(forKey key: String) -> String? {
        return readObject(forKey: key) as? String
    }
}

// MARK: - Clean

extension DataCache {
    /// Clean all mem cache and disk cache. This is an async operation.
    public func cleanAll() {
        cleanMemCache()
        cleanDiskCache()
    }
    
    /// Clean cache by key. This is an async operation.
    public func clean(byKey key: String) {
        memCache.removeObject(forKey: key as AnyObject)
        
        ioQueue.async {
            do {
                try self.fileManager.removeItem(atPath: self.cachePath(forKey: key))
            } catch {
                print("DataCache: Error while remove file: \(error.localizedDescription)")
            }
        }
    }
    
    public func cleanMemCache() {
        memCache.removeAllObjects()
    }
    
    public func cleanDiskCache() {
        ioQueue.async {
            do {
                try self.fileManager.removeItem(atPath: self.cachePath)
            } catch {
                print("DataCache: Error when clean disk: \(error.localizedDescription)")
            }
        }
    }
    
}

// MARK: - Helpers

extension DataCache {
    func cachePath(forKey key: String) -> String {
        let fileName = key.toBase64()
        return (cachePath as NSString).appendingPathComponent(fileName)
    }
}
