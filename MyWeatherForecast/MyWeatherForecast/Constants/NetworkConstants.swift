//
//  NetworkConstants.swift
//  MyWeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation

enum NetworkConstants {
    static let openWeatherAPIKey = "OPEN_WEATHER_API_KEY"
    static let weatherDomain = "api.openweathermap.org"
    static let baseWeatherURL = "https://\(weatherDomain)"
}
