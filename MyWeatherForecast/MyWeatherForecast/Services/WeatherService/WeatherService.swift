//
//  WeatherService.swift
//  WeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation
import Alamofire

final class WeatherService: WeatherServiceProtocol {
    var apiClient: ApiClientProtocol?
    
    init(apiClient: ApiClientProtocol) {
        self.apiClient = apiClient
    }
    
    func fetchAllWeekWeather(fromParams params: Parameters?, completion: @escaping FetchAllWeatherCompletion) {
        self.apiClient?.getAPI(url: WeatherRouter.allWeek.url, timeout: 30, isUsingCached: true, headers: nil, parameters: params, completion: { (result: Result<ApiResponse<AllWeekWeatherResponse>>) in
            switch result {
            case let .success(response):
                completion(.success(response.entity))
            case let .failure(error):
                completion(.failure(error))
            }
        })
    }
}
