//
//  WeatherServiceProtocol.swift
//  WeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation
import Alamofire

typealias FetchAllWeatherCompletion = (_ weathers: Result<AllWeekWeatherResponse>) -> Void

protocol WeatherServiceProtocol {
    func fetchAllWeekWeather(fromParams params: Parameters?, completion: @escaping FetchAllWeatherCompletion)
}
