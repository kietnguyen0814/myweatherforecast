//
//  APIResponse.swift
//  MyWeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation

typealias Result<T> = Swift.Result<T, ServiceError>

struct ApiResponse<T: Decodable> {
    let entity: T
    let data: Data?
    
    init(data: Data?) throws {
        do {
            let decoder = JSONDecoder()
            self.entity = try decoder.decode(T.self, from: data ?? Data())
            self.data = data
        } catch {
            throw ServiceError.parseDataError
        }
    }
}

public enum ServiceError: Error {
    case apiError
    case invalidResponse
    case noData
    case parseDataError
    case unknown
}
