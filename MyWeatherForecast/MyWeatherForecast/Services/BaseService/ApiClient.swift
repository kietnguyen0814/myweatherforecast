//
//  ApiClient.swift
//  MyWeatherForecast
//
//  Created by Kiet Nguyen on 3/4/22.
//

import Foundation
import Alamofire

protocol ApiClientProtocol {
    func getAPI<T: Codable>(url: String, timeout: TimeInterval, isUsingCached: Bool, headers: HTTPHeaders?, parameters: Parameters?, completion: @escaping (Result<ApiResponse<T>>) -> Void)
}

class ApiClient: NSObject {
    private func httpOperation<T: Codable>(method: HTTPMethod,
                                           url: String,
                                           timeout: TimeInterval,
                                           isUsingCached: Bool = false,
                                           headers: HTTPHeaders? = nil,
                                           parameters: Parameters? = nil,
                                           completion: @escaping (Result<ApiResponse<T>>) -> Void) {
        let urlEncoded = url.encodeUrlString(parameters: parameters)
        if let cachedResponseString = DataCache.shareInstance.readString(forKey: urlEncoded),
           let cachedData = cachedResponseString.data(using: String.Encoding.utf8) {
            do {
                let responseData = try ApiResponse<T>(data: cachedData)
                completion(.success(responseData))
            } catch (let error) {
                print(error.localizedDescription)
                completion(.failure(ServiceError.parseDataError))
            }
        } else {
            let sessionManager = AF
            sessionManager.session.configuration.timeoutIntervalForRequest = timeout
            sessionManager.request(url,
                                   method: method,
                                   parameters: parameters,
                                   encoding: URLEncoding.queryString,
                                   headers: headers)
                .validate(statusCode: 200..<300)
                .responseJSON { (responseObject) in
                    DispatchQueue.global(qos: .background).async {
                        let statusCode = responseObject.response?.statusCode
                        print("API STATUS \(String(describing: statusCode))")
                        switch responseObject.result {
                        case .success:
                            guard let data = responseObject.data else {
                                completion(.failure(ServiceError.noData))
                                return
                            }
                            do {
                                guard let json = String(data: data, encoding: String.Encoding.utf8) else {
                                    completion(.failure(ServiceError.parseDataError))
                                    return
                                }
                                if (isUsingCached) {
                                    DataCache.shareInstance.write(string: json, forKey: urlEncoded)
                                }
                                let responseData = try ApiResponse<T>(data: data)
                                completion(.success(responseData))
                            } catch (let error) {
                                print(error.localizedDescription)
                                completion(.failure(ServiceError.parseDataError))
                            }
                        case .failure:
                            completion(.failure(ServiceError.apiError))
                        }
                    }
                }
        }
    }
}

extension ApiClient: ApiClientProtocol {
    func getAPI<T: Codable>(url: String, timeout: TimeInterval, isUsingCached: Bool, headers: HTTPHeaders?, parameters: Parameters?, completion: @escaping (Result<ApiResponse<T>>) -> Void) {
        httpOperation(method: .get, url: url, timeout: timeout, isUsingCached: isUsingCached ,headers: headers, parameters: parameters, completion: completion)
    }
}
