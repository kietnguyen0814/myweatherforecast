//
//  HTTPRouterTests.swift
//  MyWeatherForecastTests
//
//  Created by Kiet Nguyen on 3/6/22.
//

import Foundation
import XCTest

@testable import MyWeatherForecast

class HTTPRouterTests: XCTestCase {
    func testGetUrl() {
        let currentUrl = WeatherRouter.current.url
        let compareCurrent = NetworkConstants.baseWeatherURL + "/data/2.5/onecall"
        
        let allWeekUrl = WeatherRouter.allWeek.url
        let compareAllWeek = NetworkConstants.baseWeatherURL + "/data/2.5/forecast/daily"
        
        XCTAssertEqual(currentUrl, compareCurrent)
        XCTAssertEqual(allWeekUrl, compareAllWeek)
    }
}
