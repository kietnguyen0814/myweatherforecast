//
//  CacheManagerTests.swift
//  MyWeatherForecastTests
//
//  Created by Kiet Nguyen on 3/6/22.
//

import Foundation
import XCTest

@testable import MyWeatherForecast

class CacheManagerTests: XCTestCase {
    override class func setUp() {
        let sut = DataCache.shareInstance
        sut.cleanAll()
    }
    
    func testDontHaveCacheDataOnDisk() {
        let sut = DataCache.shareInstance
        XCTAssertEqual(sut.readString(forKey: "empty"), nil)
    }
    
    func testHaveCacheDataOnDiskButNotOnMemory() {
        let sut = DataCache.shareInstance
        let queue = DispatchQueue(label: "test_read_disk_data")
        queue.async {
            sut.write(string: "NAB", forKey: "test disk cache")
        }
        queue.async {
            sut.cleanMemCache()
            DispatchQueue.main.async {
                XCTAssertEqual(sut.readString(forKey: "test disk cache"), "NAB")
            }
        }
    }
    
    func testWriteDataToCacheAndDisk() {
        let sut = DataCache.shareInstance
        let queue = DispatchQueue(label: "test_read_disk_data")
        queue.async {
            sut.write(string: "NAB", forKey: "test write cache")
        }
        queue.async {
            XCTAssertEqual(sut.readString(forKey: "test write cache"), "NAB")
        }
    }
    
    func testCleanDataCache() {
        let sut = DataCache.shareInstance
        let queue = DispatchQueue(label: "test_clean_data")
        queue.async {
            sut.write(string: "NAB", forKey: "test clean")
        }
        queue.async {
            sut.clean(byKey: "test clean")
        }
        queue.async {
            XCTAssertEqual(sut.readString(forKey: "test clean"), nil)
            XCTAssertNil(sut.memCache.object(forKey: "test clean" as AnyObject))
        }
    }
}
