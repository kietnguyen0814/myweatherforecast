//
//  MockWeatherService.swift
//  MyWeatherForecastTests
//
//  Created by Kiet Nguyen on 3/5/22.
//

import Foundation
import Alamofire

@testable import MyWeatherForecast

final class MockWeatherService {
    var shouldReturnError = false
    var errorType: ServiceError?
    
    init(shouldReturnError: Bool, errorType: ServiceError?) {
        self.shouldReturnError = shouldReturnError
        self.errorType = errorType
    }
    
    func reset() {
        shouldReturnError = false
        errorType = nil
    }
}
extension MockWeatherService: WeatherServiceProtocol {
    func fetchAllWeekWeather(fromParams params: Parameters?, completion: @escaping FetchAllWeatherCompletion) {
        if (shouldReturnError) {
            completion(.failure(errorType ?? .unknown))
        } else {
            completion(.success(MockAllWeekWeatherResponse.correctData))
        }
    }
}
