//
//  MockAllWeekWeatherResponse.swift
//  MyWeatherForecastTests
//
//  Created by Kiet Nguyen on 3/5/22.
//

import Foundation
@testable import MyWeatherForecast

struct MockAllWeekWeatherResponse {
    private static let firstWeatherDesc = Weather(id: 500, main: "Rain", description: "light rain")
    private static let firstTemp = Temperature(day: 28.28, min: 23.81, max: 29.01)
    static let firstDateWeather = AllWeekWeather(dt: 1630814400, temp: Self.firstTemp, pressure: 1008, humidity: 74, speed: 2.16, weather: [Self.firstWeatherDesc])
    
    private static let secondWeatherDesc = Weather(id: 500, main: "Cloud", description: "no rain")
    private static let secondTemp = Temperature(day: 28.28, min: 23.81, max: 29.01)
    private static let secondDateWeather = AllWeekWeather(dt: 1630814500, temp: Self.firstTemp, pressure: 1008, humidity: 74, speed: 2.16, weather: [Self.secondWeatherDesc])
    
    static let correctData: AllWeekWeatherResponse = AllWeekWeatherResponse(list: [Self.firstDateWeather, Self.secondDateWeather])
}
