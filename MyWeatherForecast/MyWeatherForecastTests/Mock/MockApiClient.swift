//
//  MockApiClient.swift
//  MyWeatherForecastTests
//
//  Created by Kiet Nguyen on 3/6/22.
//

import Foundation
import Alamofire

@testable import MyWeatherForecast

final class MockApiClient: ApiClientProtocol {
    var shouldReturnError = false
    var errorType: ServiceError?
    
    init(shouldReturnError: Bool, errorType: ServiceError?) {
        self.shouldReturnError = shouldReturnError
        self.errorType = errorType
    }
    
    func getAPI<T: Codable>(url: String, timeout: TimeInterval, isUsingCached: Bool, headers: HTTPHeaders?, parameters: Parameters?, completion: @escaping (Result<ApiResponse<T>>) -> Void) {
        if (shouldReturnError) {
            completion(.failure(errorType ?? .unknown))
        } else {
            let encoder = JSONEncoder()
            let mock = MockAllWeekWeatherResponse.correctData
            do {
                let mockData = try encoder.encode(mock)
                if (errorType != nil) {
                    let response = try ApiResponse<T>(data: nil)
                    completion(.success(response))
                } else {
                    let response = try ApiResponse<T>(data: mockData)
                    completion(.success(response))
                }
            } catch {
                completion(.failure(errorType ?? .parseDataError))
            }
        }
    }
}
