//
//  WeatherServiceTests.swift
//  MyWeatherForecastTests
//
//  Created by Kiet Nguyen on 3/6/22.
//

import Foundation
import XCTest

@testable import MyWeatherForecast

class WeatherServiceTests: XCTestCase {
    func testFetchListWeatherFail() {
        let mockApiClient = MockApiClient(shouldReturnError: true, errorType: .apiError)
        let sut = WeatherService(apiClient: mockApiClient)
        sut.fetchAllWeekWeather(fromParams: nil) { response in
            switch response {
            case .success(_): break
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
    
    func testFetchListWeatherSuccess() {
        let mockApiClient = MockApiClient(shouldReturnError: false, errorType: nil)
        let sut = WeatherService(apiClient: mockApiClient)
        sut.fetchAllWeekWeather(fromParams: nil) { result in
            switch result {
            case .success(let response):
                XCTAssertNotNil(response)
            case .failure(_): break
            }
        }
    }
    
    func testFetchListWeatherSuccessButCantParseData() {
        let mockApiClient = MockApiClient(shouldReturnError: false, errorType: .parseDataError)
        let sut = WeatherService(apiClient: mockApiClient)
        sut.fetchAllWeekWeather(fromParams: nil) { result in
            switch result {
            case .success(_): break
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
}
