//
//  WeatherForecastPresenterTests.swift
//  MyWeatherForecastTests
//
//  Created by Kiet Nguyen on 3/5/22.
//

import Foundation
import UIKit
import Quick
import Nimble

@testable import MyWeatherForecast

final class WeatherForecastPresenterTests: QuickSpec {
    
    override func spec() {
        let mockService = MockWeatherService(shouldReturnError: false, errorType: nil)
        let mockView = WeatherForecastViewController()
        let sut = WeatherForecastPresenter(view: mockView, weatherService: mockService)
        describe("WeatherForecastPresenter") {
            context("When Presenter is inited") {
                beforeEach {
                    sut.listWeathers.removeAll()
                }
                it("And get the empty listCities", closure: {
                    expect(sut.numbersWeatherInSection).to(equal(0))
                    expect(sut.listWeathers.count).to(equal(0))
                })
            }
            
            context("When call API") {
                beforeEach {
                    sut.listWeathers.removeAll()
                    mockService.reset()
                }
                
                it("And api key is invalid") {
                    mockService.shouldReturnError = true
                    sut.fetchDailyWeather(with: "invalid key", apiKey: "invalid")
                    expect(sut.numbersWeatherInSection).to(equal(0))
                }
                
                it("Should get empty response") {
                    mockService.shouldReturnError = true
                    mockService.errorType = .noData
                    sut.fetchDailyWeather(with: "empty response")
                    expect(sut.numbersWeatherInSection).to(equal(0))
                }
                
                it("Should fail to parse data") {
                    mockService.shouldReturnError = true
                    mockService.errorType = .parseDataError
                    sut.fetchDailyWeather(with: "parse error")
                    expect(sut.numbersWeatherInSection).to(equal(0))
                }
                
                it("Should return failed when out of range", closure: {
                    let indexPath: IndexPath = IndexPath(item: -1, section: 0)
                    expect(sut.weatherForRowIndexPath(indexPath: indexPath)).to(beNil())
                })
                
                it("Should get correct dt by index", closure: {
                    let indexPath: IndexPath = IndexPath(item: 0, section: 0)
                    sut.fetchDailyWeather(with: "Ho Chi Minh")
                    expect(sut.weatherForRowIndexPath(indexPath: indexPath)?.dt)
                        .to(equal(1630814400))
                })
            }
        }
    }
    
}
