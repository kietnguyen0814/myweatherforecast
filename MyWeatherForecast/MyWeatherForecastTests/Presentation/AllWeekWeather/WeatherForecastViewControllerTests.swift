//
//  WeatherForecastViewControllerTests.swift
//  MyWeatherForecastTests
//
//  Created by Kiet Nguyen on 3/6/22.
//

import Foundation
import UIKit
import Quick
import Nimble

@testable import MyWeatherForecast

final class WeatherForecastViewControllerTestsTemp: XCTestCase {
    
    func createSut() -> WeatherForecastViewController {
        let sut = WeatherForecastViewController()
        sut.loadViewIfNeeded()
        let mockService = MockWeatherService(shouldReturnError: false, errorType: nil)
        let mockPresenter = WeatherForecastPresenter(view: sut, weatherService: mockService)
        sut.presenter = mockPresenter
        return sut
    }
    
    func testUserInput2Characters() {
        let sut = createSut()
        sut.txtSearch.text = "Ho"
        sut.searchBarSearchButtonClicked(sut.txtSearch)
        XCTAssertEqual(sut.presenter?.numbersWeatherInSection, 0)
    }
    
    func testUserInput2CharactersAndTapCancel() {
        let sut = createSut()
        sut.txtSearch.text = "Ho"
        sut.searchBarCancelButtonClicked(sut.txtSearch)
        XCTAssertEqual(sut.presenter?.numbersWeatherInSection, 0)
    }
    
    func testUserInputMoreThan3CharacterAndGetSuccessData() {
        let sut = createSut()
        sut.searchBarTextDidBeginEditing(sut.txtSearch)
        sut.txtSearch.text = "Ho Chi Minh"
        sut.searchBarSearchButtonClicked(sut.txtSearch)
        XCTAssertEqual(sut.presenter?.numbersWeatherInSection, 2)
    }
    
    func testUserInputMoreThan3CharacterAndGetNoData() {
        let sut = WeatherForecastViewController()
        sut.loadViewIfNeeded()
        let mockService = MockWeatherService(shouldReturnError: false, errorType: nil)
        let mockPresenter = WeatherForecastPresenter(view: sut, weatherService: mockService)
        sut.presenter = mockPresenter
        mockService.shouldReturnError = true
        mockService.errorType = .noData
        
        sut.searchBarTextDidBeginEditing(sut.txtSearch)
        sut.txtSearch.text = "Hochiminh"
        sut.searchBarSearchButtonClicked(sut.txtSearch)
        XCTAssertEqual(mockPresenter.numbersWeatherInSection, 0)
    }
}
