//
//  AllWeekWeatherTests.swift
//  MyWeatherForecastTests
//
//  Created by Kiet Nguyen on 3/6/22.
//

import Foundation
import XCTest

@testable import MyWeatherForecast

class AllWeekWeatherTests: XCTestCase {
    func testGetStringDate() {
        let sut: AllWeekWeather = MockAllWeekWeatherResponse.firstDateWeather
        let shortDate = sut.strShortDate
        let fullDate = sut.strFullDate
        XCTAssertEqual(shortDate, "Sun, 5 Sep 2021")
        XCTAssertEqual(fullDate, "Sunday, Sep 5, 2021")
    }
}
