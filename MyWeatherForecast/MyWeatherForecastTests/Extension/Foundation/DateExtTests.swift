//
//  DateExtTests.swift
//  MyWeatherForecastTests
//
//  Created by Kiet Nguyen on 3/6/22.
//

import Foundation
import XCTest

@testable import MyWeatherForecast

class DateExt: XCTestCase {
    func testDateToString() {
        let date = Date(timeIntervalSince1970: TimeInterval(1646586000))
        XCTAssertEqual(date.toString(format: CustomDateStyle.EdMMMyyyy.rawValue), "Mon, 7 Mar 2022")
    }
}
