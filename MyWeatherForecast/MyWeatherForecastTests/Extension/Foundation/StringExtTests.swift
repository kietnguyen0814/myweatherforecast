//
//  StringExtTests.swift
//  MyWeatherForecastTests
//
//  Created by Kiet Nguyen on 3/6/22.
//

import Foundation
import Alamofire
import XCTest

@testable import MyWeatherForecast

class StringExtTests: XCTestCase {
    func testEncodeStringBase64() {
        let string = "NAB"
        XCTAssertEqual(string.toBase64(), "TkFC")
    }
    
    func testDecodeStringToBase64() {
        let string = "TkFC"
        XCTAssertEqual(string.fromBase64(), "NAB")
    }
    
    func testEncodeUrlString() {
        guard let apiKey = Bundle.main.getInfoByKey(key: NetworkConstants.openWeatherAPIKey) as? String else { return }
        let url = "https://api.openweathermap.org/data/2.5/forecast/daily"
        let param: Parameters = [
            "q": "melbourne",
            "cnt": 7,
            "appid": apiKey,
            "units": "metric"
        ]
        let encodedString = url.encodeUrlString(parameters: param)
        let encodeEmptyParam = url.encodeUrlString(parameters: nil)
        XCTAssertEqual(encodedString, "https://api.openweathermap.org/data/2.5/forecast/daily?appid=8d03bb5bf36b834fe4842f00333c5788&cnt=7&q=melbourne&units=metric")
        XCTAssertEqual(encodeEmptyParam, url)
    }
}
