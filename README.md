# Weather Forecast

- The Weather Forecast app shows weather information for today and the next days
- Using open API of openweathermap.org to get weather data

## Getting Started

- Clone the project
- Install Xcode 12
- Open WeatherForecast.xcworkspace
- Chose simulator and run

***

## Project Structure

### Weather Forecast

- **Constants** folder contains the default project variables
- **Extensions** folder contains extensions that swift hasn't supported yet
- **Services** folder contains classes or structures that make API calls
- **Models** folder contains classes or structures that hold data
- **Utils** folder contains utility classes that are used throughout the project
- **Presentation** folder contains View and Presneter classes of each screen
- **Resources** folder contains installation files, configuration of the project

### WeatherForecastTests

- **Services** folder contains  UnitTest classes for Service of the app
- **Model** folder contains  UnitTest classes for Model of the app
- **Utils** folder contains  UnitTest classes for utilities of the app
- **Extension** folder contains  UnitTest classes for extensions of the app
- **Mock** folder contains  all the mock which is inherited from the interface
- **Presentation** folder contains all the mock data and UnitTest classes of the project

### Frameworks
- **Alamofire** is a Swift-based HTTP networking library for iOS. It provides an elegant interface on top of Apple's Foundation networking stack that simplifies a number of common networking tasks.
- **Kingfisher** is a pure-Swift library for downloading and caching images from the web
- **Quick** is a behavior-driven development framework for Swift and Objective-C.
- **Nimble** express the expected outcomes of Swift or Objective-C expressions

***

## General Architectural Approach

### MVP Concepts

- `Model` should be *autonomous*, i.e. you should be able to use the same code for the model for a command line application and a UI interface. It will take care of all the business logic.
- `View` - delegates user interaction events to the `Presenter` and displays data passed by the `Presenter`
- `Presenter` - contains the presentation logic and tells the `View` what to present

## Done

- [x] Develop the project by using Swift
- [x] Design app's architecture with MVP
- [x] The application is a simple iOS application that is written by Swift.
- [x] The application is able to retrieve the weather information from OpenWeatherMaps API. 
- [x] The application is able to allow user to input the searching term.
- [x] The application is able to proceed searching with a condition of the search term length must be from 3 characters or above.
- [x] The application is able to render the searched results as a list of weather items.
- [x] The application is able to support caching mechanism so as to prevent the app from generating a bunch of API requests.
- [x] The application is able to manage caching mechanism & lifecycle.  
- [x] The application is able to handle failures.
- [x] The application is able to support the disability to scale large text for who can't see the text clearly
- [x] The application is able to support the disability to read out the text using VoiceOver controls.
- [x] Write UnitTests
***